import os

from bokeh.io import curdoc
from bokeh.io.state import curstate
from bokeh.models import Button, Div
from bokeh.layouts import column

from time import sleep


app_path = os.path.dirname(__file__)
static_path = os.path.join(app_path,'static')


with open(os.path.join(static_path,'loader.html'),'r') as infile:
    loader_css = infile.read().replace('\n','')


def busy(func):
    '''
    Decorator function to display a loading animation when the program is working on something
    '''
    def wrapper():
        # show the loading animation for time consuming tasks
        curdoc().select_one({'name':'loading_animation'}).visible = True
        do_stuff_button = curdoc().select_one({'name':'do_stuff_button'})
        do_stuff_button.disabled = True
        do_stuff_button.label = "Doing stuff"
        curdoc().add_next_tick_callback(func)
    return wrapper


@busy
def do_stuff():
    sleep(5)
    do_stuff_button = curdoc().select_one({'name':'do_stuff_button'})
    do_stuff_button.disabled = False
    do_stuff_button.label = "Do stuff"
    curdoc().select_one({'name':'loading_animation'}).visible = False


loader = Div(text=loader_css,width=40,height=40,name="loading_animation",visible=False)

do_stuff_button = Button(label="Do stuff",width=100,css_classes=["custom_button"],name="do_stuff_button")

do_stuff_button.on_click(do_stuff)

curdoc().add_root(column(do_stuff_button,loader))
